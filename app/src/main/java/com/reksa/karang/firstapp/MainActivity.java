package com.reksa.karang.firstapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TextView tvNumberCount;
    Button btnToast, btnCount, btnRandom;
    int clickCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvNumberCount = findViewById(R.id.tv_number_count);
        btnToast = findViewById(R.id.btn_toast);
        btnCount= findViewById(R.id.btn_count);
        btnRandom= findViewById(R.id.btn_random);

        btnToast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "" + tvNumberCount.getText(), Toast.LENGTH_SHORT).show();
            }
        });

        btnCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickCount = clickCount + 1;
                tvNumberCount.setText(String.valueOf(clickCount));
            }
        });

        btnRandom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random random = new Random();
                int n = random.nextInt(100) + 1;
                tvNumberCount.setText(String.valueOf(n));
            }
        });

    }
}
